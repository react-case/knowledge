import { generate as uuid } from 'shortid';


const todos = [
  { id: uuid(), destroyed: false, dt: '2022-10-05T08:00:00.000Z', done: true, content: 'Get Up' },
  { id: uuid(), destroyed: false, dt: '2022-10-05T09:00:00.000Z', done: false, content: 'Set Off' },
  { id: uuid(), destroyed: false, dt: '2022-10-05T09:40:00.000Z', done: false, content: 'Side Layout' },
  { id: uuid(), destroyed: false, dt: '2022-10-05T10:00:00.000Z', done: false, content: 'Kick Off' }
];

const fetcher = {
  search: () => (
    new Promise((resolve) => (
      setTimeout(() => resolve(todos.filter(({ destroyed }) => !destroyed)), 1000)
    ))
  ),
  add: (data) => (
    new Promise((resolve) => (
      setTimeout(() => {
        todos.push(data);
        resolve();
      }, 1000)
    ))
  ),
  update: (id, done) => (
    new Promise((resolve) => (
      setTimeout(() => {
        todos.forEach((todo) => {
          if (todo.id === id) {
            todo.done = done;
          }
        });

        resolve();
      }, 1000)
    ))
  ),
  remove: (id) => (
    new Promise((resolve) => (
      setTimeout(() => {
        todos.forEach((todo) => {
          if (todo.id === id) {
            todo.destroyed = true;
          }
        });

        resolve();
      }, 1000)
    ))
  )
};

export default fetcher;
