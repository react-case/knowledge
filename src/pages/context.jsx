import { useMemo, useState } from 'react';
import { useRouter } from 'next/router';

import Container from '@material-ui/core/Container';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import { makeStyles } from '@material-ui/core/styles';

import { generate as uuid } from 'shortid';

import DocumentHead from '@features/app/DocumentHead';
import SourceTreeView from '@features/context/SourceTreeView';
import Subject from '@components/title/Subject';
import SyntaxBar from '@components/bar/SyntaxBar';


//* Custom Hook
const useStyles = makeStyles((theme) => ({
  source: {
    minHeight: theme.spacing(30),
    maxHeight: theme.spacing(60),
    overflow: 'auto'
  }
}));


//* Component
export default function Context() {
  const { pathname } = useRouter();
  const [withMemo, setWithMemo] = useState(false);

  const refresh = useMemo(() => uuid(), [withMemo]);
  const classes = useStyles();

  return (
    <>
      <DocumentHead title="Context" />

      <Subject variant="h4" color="primary">
        Context
      </Subject>

      <Container disableGutters maxWidth="sm">
        <SourceTreeView key={refresh} withMemo={withMemo} className={classes.source} {...__WEBPACK_DEFINE__.SOURCE_STRUCTURE} />
      </Container>

      <SyntaxBar source={__WEBPACK_DEFINE__.SOURCE_CODE[pathname]}>
        <FormControlLabel
          label="Use React.memo"
          labelPlacement="end"
          control={(
            <Switch color="secondary" checked={withMemo} onChange={({ target }) => setWithMemo(target.checked)} />
          )}
        />
      </SyntaxBar>
    </>
  );
}
