import Head from 'next/head';
import PropTypes from 'prop-types';


export default function DocumentHead({ title }) {
  return (
    <Head>
      <title>
        {title}
      </title>

      <link rel="icon" href="/favicon.ico" />
    </Head>
  );
}

DocumentHead.propTypes = {
  title: PropTypes.string.isRequired
};
