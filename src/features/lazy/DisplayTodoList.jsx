import List from '@material-ui/core/List';
import PropTypes from 'prop-types';
import { Suspense, lazy, useMemo } from 'react';

import DisplayTodoSkeleton from '@components/skeleton/DisplayTodoSkeleton';
import fetcher from '@services/todo';

import DisplayTodoItem from './DisplayTodoItem';


//* Custom Hook
const useLazyTodoFetch = () => (
  useMemo(() => (
    lazy(() => (
      fetcher.search()
        .then((todos) => ({
          default: (props) => (
            todos.map((data) => (
              <DisplayTodoItem key={data.id} {...props} data={data} />
            ))
          )
        }))
    ))
  ), [])
);

//* Component
export default function DisplayTodoList({ className }) {
  const LazyTodoItems = useLazyTodoFetch();

  return (
    <List className={className} style={{ position: 'relative' }}>
      <Suspense fallback={(<DisplayTodoSkeleton size={6} />)}>
        <LazyTodoItems />
      </Suspense>
    </List>
  );
}

DisplayTodoList.propTypes = {
  className: PropTypes.string
};
