import PropTypes from 'prop-types';

import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Skeleton from '@material-ui/lab/Skeleton';


export default function EditTodoSkeleton({ size: length = 10 }) {
  return (
    <>
      {Array.from({ length }).map((_e, i) => (
        <ListItem key={`skeleton_${i}`} style={{ paddingRight: 120 }}>
          <ListItemIcon>
            <Skeleton variant="rect" width={24} height={24} style={{ marginLeft: 8 }} />
          </ListItemIcon>

          <ListItemText
            disableTypography
            primary={(
              <Skeleton variant="text" width="60%" />
            )}
            secondary={(
              <Skeleton variant="text" />
            )}
          />

          <ListItemSecondaryAction>
            <Skeleton variant="circle" width={40} height={40} />
          </ListItemSecondaryAction>
        </ListItem>
      ))}
    </>
  );
}

EditTodoSkeleton.propTypes = {
  size: PropTypes.number
};
