import PropTypes from 'prop-types';

import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import withWidth from '@material-ui/core/withWidth';

import TextTransformButton from './TextTransformButton';


const RwdButton = ({
  children,
  color = 'default',
  disabled = false,
  fullWidth = false,
  icon,
  iconPosition = 'start',
  size,
  variant,
  width,

  onClick,
  ...props
}) => (
  width === 'xs'
    ? (
      <Tooltip title={children || ''} {...(!children && { open: false })}>
        <IconButton {...{ color, disabled, onClick }} {...props} size={size === 'large' ? 'medium' : size}>
          {icon}
        </IconButton>
      </Tooltip>
    )
    : (
      <TextTransformButton
        {...props}
        {...{ color, disabled, fullWidth, size, variant, onClick }}
        {...{ [`${iconPosition}Icon`]: icon }}
      >
        {children}
      </TextTransformButton>
    )
);

RwdButton.propTypes = {
  children: PropTypes.node,
  color: PropTypes.oneOf(['default', 'inherit', 'primary', 'secondary']),
  disabled: PropTypes.bool,
  fullWidth: PropTypes.bool,
  icon: PropTypes.element,
  iconPosition: PropTypes.oneOf(['end', 'start']),
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  variant: PropTypes.oneOf(['contained', 'outlined', 'text']),

  onClick: PropTypes.func
};

export default withWidth()(RwdButton);
