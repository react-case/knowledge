import PropTypes from 'prop-types';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';

import color from 'randomcolor';


export default function PureColorPanel({ description }) {
  const background = color();

  return (
    <Card>
      <CardHeader title="Child Node" subheader={description} />
      <CardContent style={{ background }} />
    </Card>
  );
}

PureColorPanel.propTypes = {
  description: PropTypes.string.isRequired
};
