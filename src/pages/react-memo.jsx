import DocumentHead from '@features/app/DocumentHead';
import { memo, useState } from 'react';
import { useRouter } from 'next/router';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import Paper from '@material-ui/core/Paper';
import Slide from '@material-ui/core/Slide';
import Switch from '@material-ui/core/Switch';

import ColorPanel from '@features/memo/ColorPanel';
import Subject from '@components/title/Subject';
import SyntaxBar from '@components/bar/SyntaxBar';


//* Components
const MemoColorPanel = memo(ColorPanel);

export default function ReactMemo() {
  const { pathname } = useRouter();
  const [withMemo, setWithMemo] = useState(false);

  return (
    <>
      <DocumentHead title="React.memo" />

      <Subject variant="h4" color="primary">
        React.memo
      </Subject>

      <Slide direction="right" in={withMemo}>
        <Paper style={{ display: withMemo ? 'block' : 'none' }}>
          <MemoColorPanel withMemo />
        </Paper>
      </Slide>

      <Slide direction="left" in={!withMemo}>
        <Paper style={{ display: !withMemo ? 'block' : 'none' }}>
          <MemoColorPanel />
        </Paper>
      </Slide>

      <SyntaxBar source={__WEBPACK_DEFINE__.SOURCE_CODE[pathname]}>
        <FormControlLabel
          label="With React.memo"
          labelPlacement="end"
          control={(
            <Switch color="secondary" checked={withMemo} onChange={({ target }) => setWithMemo(target.checked)} />
          )}
        />
      </SyntaxBar>
    </>
  );
}
