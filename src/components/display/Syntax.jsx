import PropTypes from 'prop-types';
import Syntax from 'react-syntax-highlighter';
import dynamic from 'next/dynamic';
import { useState } from 'react';

import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Toolbar from '@material-ui/core/Toolbar';
import { makeStyles } from '@material-ui/core/styles';


//* Custom Hooks
const useStyles = makeStyles((theme) => ({
  tab: {
    textTransform: 'capitalize'
  }
}));


//* Components
const DynamicSyntax = dynamic(
  () => (
    import('react-syntax-highlighter/dist/esm/styles/hljs')
      .then(({ nightOwl }) => (
        function DynamicSyntax({ children }) {
          return (
            <Syntax showLineNumbers language="javascript" style={nightOwl}>
              {children}
            </Syntax>
          );
        }
      ))
  ),
  { ssr: false }
);

export default function SyntaxTabs({ options }) {
  const [actived, setActived] = useState(0);
  const classes = useStyles();

  return (
    <>
      <AppBar position="static" color="default">
        <Toolbar variant="dense">
          <Tabs
            variant="scrollable"
            scrollButtons="auto"
            indicatorColor="secondary"
            textColor="secondary"
            value={actived}
            onChange={(_e, newActived) => setActived(newActived)}
          >
            {options.map(({ title }) => (
              <Tab key={title} label={title} classes={{ root: classes.tab }} />
            ))}
          </Tabs>
        </Toolbar>
      </AppBar>

      <DynamicSyntax>
        {options[actived].src}
      </DynamicSyntax>
    </>
  );
}

SyntaxTabs.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.exact({
      title: PropTypes.string.isRequired,
      src: PropTypes.string.isRequired
    })
  )
};
