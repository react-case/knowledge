import PropTypes from 'prop-types';
import { memo, useMemo } from 'react';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import TreeView from '@material-ui/lab/TreeView';
import Typography from '@material-ui/core/Typography';

import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';

import FolderTreeItem from '@components/tree/FolderTreeItem';
import SelectionProvider, { useProviderValue } from '@utils/selection-context';


export default function SourceTreeView({ className, label, items, withMemo = false }) {
  const FolderElement = useMemo(() => withMemo ? memo(FolderTreeItem) : FolderTreeItem, [withMemo]);
  const value = useProviderValue();

  return (
    <SelectionProvider value={value}>
      <AppBar color="transparent" position="static" elevation={0}>
        <Toolbar disableGutters variant="dense">
          <Typography variant="subtitle1">
            Selected File Count: {value.selecteds.size}
          </Typography>
        </Toolbar>
      </AppBar>

      <TreeView
        className={className}
        defaultCollapseIcon={<KeyboardArrowDownIcon />}
        defaultExpandIcon={<KeyboardArrowRightIcon />}
        selected={Array.from(value.selecteds)}
      >
        <FolderElement {...{ label, items }} />
      </TreeView>
    </SelectionProvider>
  );
}

SourceTreeView.propTypes = {
  className: PropTypes.string,
  label: PropTypes.string.isRequired,
  withMemo: PropTypes.bool,

  items: PropTypes.arrayOf(
    PropTypes.object.isRequired
  ).isRequired
};
