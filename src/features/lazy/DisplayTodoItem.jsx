import PropTypes from 'prop-types';
import { useMemo } from 'react';

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';


export default function DisplayTodoItem({ data: { dt, content } }) {
  const date = useMemo(() => new Date(dt), [dt]);

  return (
    <ListItem>
      <ListItemText
        primary={content}
        secondary={date.toString() === 'Invalid Date' ? 'None' : date.toLocaleString()}
      />
    </ListItem>
  );
}

DisplayTodoItem.propTypes = {
  data: PropTypes.shape({
    dt: PropTypes.string,
    content: PropTypes.string
  }).isRequired
};
