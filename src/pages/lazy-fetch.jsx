import DocumentHead from '@features/app/DocumentHead';
import { useRouter } from 'next/router';
import { useState } from 'react';
import { generate as uuid } from 'shortid';

import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';

import RefreshIcon from '@material-ui/icons/Refresh';

import Button from '@components/button/TextTransformButton';
import DisplayTodoList from '@features/lazy/DisplayTodoList';
import Subject from '@components/title/Subject';
import SyntaxBar from '@components/bar/SyntaxBar';


//* Custom Hook
const useStyles = makeStyles((theme) => ({
  todo: {
    display: 'flex',
    flexDirection: 'column',
    maxHeight: theme.spacing(36),
    overflow: 'hidden auto'
  }
}));

//* Component
export default function LazyFetch() {
  const { pathname } = useRouter();
  const [refresh, setRefresh] = useState(uuid());
  const classes = useStyles();

  return (
    <>
      <DocumentHead title="Lazy Fetch" />

      <Subject variant="h4" color="primary">
        Lazy Fetch Data
      </Subject>

      <Container disableGutters maxWidth="sm">
        <DisplayTodoList key={refresh} className={classes.todo} />
      </Container>

      <SyntaxBar source={__WEBPACK_DEFINE__.SOURCE_CODE[pathname]}>
        <Button
          color="secondary"
          size="large"
          variant="text"
          startIcon={(<RefreshIcon />)}
          onClick={() => setRefresh(uuid())}
        >
          Reload
        </Button>
      </SyntaxBar>
    </>
  );
}
