import NextLink from 'next/link';
import PropTypes from 'prop-types';

import EmojiObjectsOutlinedIcon from '@material-ui/icons/EmojiObjectsOutlined';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';

import cx from 'clsx';


const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    fontWeight: 'bolder',
    fontSize: theme.typography.h5.fontSize,

    [theme.breakpoints.only('xs')]: {
      fontSize: theme.typography.h6.fontSize
    }
  }
}));

export default function Logo({ className, onClick }) {
  const classes = useStyles();

  return (
    <NextLink href="/">
      <Link className={cx(classes.root, className)} underline="none" variant="h5" color="primary" href="/" onClick={onClick}>
        <EmojiObjectsOutlinedIcon role="icon" fontSize="inherit" color="primary" />

        React Knowledge
      </Link>
    </NextLink>
  );
}

Logo.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func
};
