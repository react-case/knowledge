import { Fragment } from 'react';
import { ThemeProvider, createTheme, withStyles } from '@material-ui/core/styles';


const DEFAULT_THEME = createTheme({
  palette: {
    type: 'dark',
    background: {
      default: '#252C2B',
      paper: '#36413E'
    },
    primary: {
      light: '#78B7BA',
      main: '#56A3A6',
      dark: '#4C9294'
    },
    secondary: {
      light: '#F7E08D',
      main: '#F4D35E',
      dark: '#F2CB40'
      // light: '#FFA585',
      // main: '#FF875C',
      // dark: '#FF6933'
    },
    info: {
      light: '#C5E0E7',
      main: '#A8D0DB',
      dark: '#8CC0CF'
    },
    success: {
      light: '#E9FFAD',
      main: '#DBFF76',
      dark: '#D3FF5C'
    },
    warning: {
      light: '#FDB321',
      main: '#F0A202',
      dark: '#CA8702'
    },
    error: {
      light: '#F25467',
      main: '#EF233C',
      dark: '#E4112A'
    }
  },
  typography: {
    fontFamily: 'Noto Sans TC,Open Sans,sans-serif'
  }
});

//* Components
const GlobalStyles = withStyles((theme) => ({
  '@global': {
    body: {
      background: theme.palette.background.default,
      color: theme.palette.getContrastText(theme.palette.background.default),
      margin: 0,
      overflow: 'hidden auto !important',
      '-webkit-user-select': 'none',
      '-moz-user-select': 'none',

      '& > div#__next': {
        display: 'flex',
        flexDirection: 'column',
        width: '100vw'
      }
    },
    '::-webkit-scrollbar': { //* 網頁捲軸【寬度】
      width: theme.spacing(1),
      height: theme.spacing(1)
    },
    '::-webkit-scrollbar-track': { //* 網頁捲軸【背景】顏色
      background: theme.palette.background.paper,
      borderRadius: theme.shape.borderRadius,
    },
    '::-webkit-scrollbar-thumb': { //* 網頁捲軸【把手】顏色
      background: theme.palette.secondary.dark,
      borderRadius: theme.shape.borderRadius
    },
    '::-webkit-scrollbar-thumb:hover': { //* 網頁捲軸【滑過時】把手的顏色
      background: theme.palette.secondary.light
    }
  }
}))(() => null);

export default function KnowledgeThemeWrapper({ children }) {
  return (
    <ThemeProvider theme={DEFAULT_THEME}>
      <GlobalStyles />

      {children}
    </ThemeProvider>
  );
}
