import PropTypes from 'prop-types';
import { Suspense, useEffect, useMemo, useRef } from 'react';

import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import IconButton from '@material-ui/core/IconButton';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import TextField from '@material-ui/core/TextField';
import { useTheme } from '@material-ui/core/styles';

import EditTodoSkeleton from '@components/skeleton/EditTodoSkeleton';
import fetcher from '@services/todo';
import { useLazyAction, useLazyFetchDispatch } from '@utils/lazy-hooks';


//* Custom Hooks
const useAddButton = (uid, data) => (
  useLazyAction(
    uid,
    () => Promise.resolve({ default: IconButton }),
    () => fetcher.add(data)
  )
);

const useDoneCheckbox = (uid, id) => (
  useLazyAction(
    uid,
    () => import('@material-ui/core/Checkbox'),

    (dispatch, { target: { checked } }) => (
      fetcher.update(id, checked).then(() => (
        dispatch({
          type: 'change',
          options: { uid, data: { done: checked } }
        })
      ))
    )
  )
);

const useDestroyButton = (uid, id) => (
  useLazyAction(
    uid,
    () => Promise.resolve({ default: IconButton }),

    (dispatch) => (
      fetcher.remove(id).then(() => (
        dispatch({
          type: 'destroy',
          options: uid
        })
      ))
    )
  )
);


//* Component
export default function EditTodoItem({ uid, status, data }) {
  const { id, content, done, dt } = data;

  const [LazyAddButton , handleAdd] = useAddButton(uid, data);
  const [LazyDoneCheckbox, handleUpdate] = useDoneCheckbox(uid, id);
  const [LazyDestroyButton, handleRemove] = useDestroyButton(uid, id);

  const dispatch = useLazyFetchDispatch();
  const date = useMemo(() => new Date(dt), [dt]);
  const itemRef = useRef();
  const theme = useTheme();

  useEffect(() => {
    if (status === 'add') {
      itemRef.current?.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }
  }, []);

  return (
    <Suspense fallback={(<EditTodoSkeleton size={1} />)}>
      <ListItem ref={itemRef} style={{ paddingRight: 120 }}>
        <ListItemIcon>
          {status !== 'add' && (
            <LazyDoneCheckbox color="primary" checked={done} onChange={handleUpdate} />
          )}
        </ListItemIcon>

        <ListItemText
          secondary={date.toString() === 'Invalid Date' ? 'None' : date.toLocaleString()}
          primary={status !== 'add'
            ? content
            : (
              <TextField
                fullWidth
                size="small"
                variant="standard"
                label="Content"
                defaultValue={content}
                onChange={(e) => dispatch({ type: 'change', options: { uid, data: { content: e.target.value } } })}
              />
            )
          }
        />

        <ListItemSecondaryAction>
          {status !== 'add'
            ? (
              <LazyDestroyButton onClick={handleRemove} style={{ color: theme.palette.error.main }}>
                <DeleteOutlineIcon />
              </LazyDestroyButton>
            )
            : (
              <>
                <IconButton color="default" onClick={() => dispatch({ type: 'destroy', options: uid })}>
                  <CloseIcon />
                </IconButton>

                <LazyAddButton color="primary" style={{ color: theme.palette.success.main }} onClick={handleAdd}>
                  <CheckIcon />
                </LazyAddButton>
              </>
            )
          }
        </ListItemSecondaryAction>
      </ListItem>
    </Suspense>
  );
}

EditTodoItem.propTypes = {
  status: PropTypes.oneOf(['add', 'destroyed', 'fetched', 'modified']),
  uid: PropTypes.string.isRequired,

  data: PropTypes.shape({
    content: PropTypes.string,
    destroyed: PropTypes.bool.isRequired,
    done: PropTypes.bool.isRequired,
    dt: PropTypes.string,
    id: PropTypes.string.isRequired
  }).isRequired
};
