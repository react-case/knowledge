import PropTypes from 'prop-types';
import { memo, useMemo, useState } from 'react';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import ColorLensOutlinedIcon from '@material-ui/icons/ColorLensOutlined';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

import color from 'randomcolor';
import { generate as uuid } from 'shortid';

import PureColorPanel from './PureColorPanel';


export default function ColorPanel({ withMemo = false }) {
  const [, setRefresh] = useState(uuid());
  const background = color();

  //* Override the target component with React.memo by the prop(withMemo).
  const ChildNodeElement = useMemo(() => (
    withMemo ? memo(PureColorPanel) : PureColorPanel
  ), [withMemo]);

  return (
    <Card>
      <CardHeader
        title="Parent Node"
        action={(
          <Tooltip title="Update State">
            <IconButton color="default" onClick={() => setRefresh(uuid())}>
              <ColorLensOutlinedIcon />
            </IconButton>
          </Tooltip>
        )}
      />

      <CardContent style={{ background }}>
        <ChildNodeElement description={withMemo ? 'With React.memo' : 'Normal'} />
      </CardContent>
    </Card>
  );
}

ColorPanel.propTypes = {
  withMemo: PropTypes.bool
};
