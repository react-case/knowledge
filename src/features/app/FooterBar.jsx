import AppBar from '@material-ui/core/AppBar';
import Avatar from '@material-ui/core/Avatar';
import SvgIcon from '@material-ui/core/SvgIcon';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

import cx from 'clsx';

import NEXT_ICON from '@assets/images/next-logo.svg';
import REACT_ICON from '@assets/images/react-logo.svg';


const COLLABORATORS = [
  { name: 'John Chasm', src: 'https://t.ly/qG9l', href: 'https://gitlab.com/VeinMudra' },
  { name: 'Jordan Tseng', src: 'https://t.ly/Ln0_', href: 'https://gitlab.com/jordantseng1' },
  { name: 'Taco Chang', src: 'https://t.ly/z1_x', href: 'https://gitlab.com/tabacotaco' },
];


//* Custom Hooks
const useStyles = makeStyles((theme) => ({
  root: {
    background: theme.palette.background.default,
    transform: 'rotate(0.5turn)',
    padding: theme.spacing(3, 0, 2, 0),

    '& > [role=toolbar]': {
      justifyContent: 'space-between',
      transform: 'rotate(0.5turn)'
    }
  },
  subtoolbar: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',

    '& > [role=content] > *': {
      margin: theme.spacing(0, 1)
    }
  },
  collaborator: {
    width: theme.spacing(5),
    height: theme.spacing(5),

    '&:hover': {
      transform: 'scale(1.2)'
    }
  }
}));


//* Component
export default function FooterBar({ className }) {
  const classes = useStyles();

  return (
    <AppBar position="static" className={cx(classes.root, className)}>
      <Toolbar role="toolbar">
        <Toolbar disableGutters className={classes.subtoolbar}>
          <Typography variant="overline" color="textSecondary">
            Collaborators
          </Typography>

          <Toolbar role="content" disableGutters variant="dense">
            {COLLABORATORS.map(({ name, ...props }) => (
              <Tooltip key={name} title={name}>
                <Avatar {...props} className={classes.collaborator} component="a" alt={name} target="_blank" />
              </Tooltip>
            ))}
          </Toolbar>
        </Toolbar>

        <Toolbar disableGutters className={classes.subtoolbar}>
          <Typography variant="overline" color="textSecondary">
            Power By
          </Typography>

          <Toolbar role="content" disableGutters variant="dense">
            <SvgIcon fontSize="large" viewBox="0 0 512 512" color="primary" component={REACT_ICON} />
            <SvgIcon fontSize="large" viewBox="0 0 1024 1024" color="action" component={NEXT_ICON} />
          </Toolbar>
        </Toolbar>
      </Toolbar>
    </AppBar>
  );
}
