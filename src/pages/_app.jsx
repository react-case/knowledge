import NextLink from 'next/link';
import { useMemo, useState } from 'react';
import { useRouter } from 'next/router';

import Container from '@material-ui/core/Container';
import NoSsr from '@material-ui/core/NoSsr';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import { makeStyles } from '@material-ui/core/styles';

import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

import cx from 'clsx';

import FooterBar from '@features/app/FooterBar';
import HeaderBar from '@features/app/HeaderBar';
import NavigationDrawer from '@features/app/NavigationDrawer';
import RwdButton from '@components/button/RwdButton';
import ThemeWrapper from '@utils/ThemeWrapper';

import ROUTES from '@assets/json/routes.json';


//* Custom Hooks
const useChapterActions = (() => {
  const FLAT_MENU_ITEMS = (function flat(items) {
    return (Array.isArray(items) ? items : []).reduce(
      (result, item) => {
        if ('pathname' in item) {
          result.push(item);
        } else if (Array.isArray(item.items)) {
          result.push(...flat(item.items));
        }

        return result;
      },
      []
    );
  })(ROUTES);

  return () => {
    const { pathname: actived } = useRouter();

    return useMemo(() => {
      const size = FLAT_MENU_ITEMS.length;
      const index = FLAT_MENU_ITEMS.findIndex(({ pathname }) => pathname === actived);

      if (global.document) {
        const [prevURL, nextURL] = [new URL(global.document?.location), new URL(global.document?.location)];
        const prev = FLAT_MENU_ITEMS[((index - 1) + size) % size];
        const next = FLAT_MENU_ITEMS[((index + 1) + size) % size];

        prevURL.pathname = `${__WEBPACK_DEFINE__.BASE_PATH}${prev.pathname}`;
        nextURL.pathname = `${__WEBPACK_DEFINE__.BASE_PATH}${next.pathname}`;
        prevURL.search = (new URLSearchParams(prev.search)).toString();
        nextURL.search = (new URLSearchParams(next.search)).toString();

        return [
          { switchable: index >= 0, prevable: index > 0, nextable: index < (size - 1) },
          { url: prevURL.toString(), text: prev.text },
          { url: nextURL.toString(), text: next.text }
        ];
      }

      return [{
        switchable: false,
        prevable: false,
        nextable: false
      }];
    }, [actived]);
  };
})();

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: `calc(100vh - ${theme.spacing(8)}px)`,
    margin: 0,

    '& > [role=content]': {
      paddingBottom: theme.spacing(4),

      '& > * + *': {
        marginTop: theme.spacing(1)
      }
    }
  },
  notXs: {
    width: '100%',

    [theme.breakpoints.up('sm')]: {
      width: `calc(100vw - ${theme.spacing(8) + 1}px)`,
      marginLeft: theme.spacing(8) + 1
    }
  },
  action: {
    marginLeft: 'auto !important',

    '& > * + *': {
      marginLeft: theme.spacing(1)
    }
  },
  footer: {
    marginTop: 'auto'
  }
}));


//* Components
export default function App({ Component, pageProps }) {
  const [open, setOpen] = useState(false);
  const [action, prev, next] = useChapterActions();
  const classes = useStyles();

  return (
    <NoSsr>
      <ThemeWrapper>
        <NavigationDrawer items={ROUTES} open={open} onMenuToggle={setOpen} />

        <HeaderBar
          className={classes.notXs}
          disableLogo={open}
          onMenuOpen={() => setOpen(true)}
          action={action.switchable && (
            <Toolbar disableGutters className={classes.action}>
              <NextLink href={prev.url}>
                <Tooltip title={prev.text}>
                  <RwdButton disabled={!action.prevable} component="a" color="default" iconPosition="start" href={prev.url} icon={(<ChevronLeftIcon />)}>
                    Prev
                  </RwdButton>
                </Tooltip>
              </NextLink>

              <NextLink href={next.url}>
                <Tooltip title={next.text}>
                  <RwdButton disabled={!action.nextable} component="a" color="default" iconPosition="end" href={next.url} icon={(<ChevronRightIcon />)}>
                    Next
                  </RwdButton>
                </Tooltip>
              </NextLink>
            </Toolbar>
          )}
        />

        <Container disableGutters maxWidth={false} className={cx(classes.container, classes.notXs)}>
          <Container role="content" maxWidth="lg">
            <Component {...pageProps} />
          </Container>

          <FooterBar className={classes.footer} />
        </Container>
      </ThemeWrapper>
    </NoSsr>
  );
}
