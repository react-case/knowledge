import PropTypes from 'prop-types';
import { useState } from 'react';

import AppBar from '@material-ui/core/AppBar';
import Collapse from '@material-ui/core/Collapse';
import Toolbar from '@material-ui/core/Toolbar';

import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

import Button from '@components/button/TextTransformButton';
import Syntax from '@components/display/Syntax';


export default function SyntaxBar({ children, source }) {
  const [open, setOpen] = useState(false);

  return (
    <>
      <AppBar position="static" color="default">
        <Toolbar variant="dense">
          {children}

          <Button
            variant="text"
            color="default"
            size="large"
            endIcon={open ? (<ExpandLess />) : (<ExpandMore />)}
            style={{ marginLeft: 'auto' }}
            onClick={() => setOpen(!open)}
          >
            View Source Code
          </Button>
        </Toolbar>
      </AppBar>

      <Collapse in={open}>
        <Syntax options={source} />
      </Collapse>
    </>
  );
}

SyntaxBar.propTypes = {
  children: PropTypes.node,
  source: PropTypes.string.isRequired
};
