import DocumentHead from '@features/app/DocumentHead';
import { useRouter } from 'next/router';
import { useState } from 'react';
import { generate as uuid } from 'shortid';

import Button from '@components/button/TextTransformButton';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';

import RefreshIcon from '@material-ui/icons/Refresh';

import EditTodoList from '@features/lazy/EditTodoList';
import Subject from '@components/title/Subject';
import SyntaxBar from '@components/bar/SyntaxBar';


//* Custom Hook
const useStyles = makeStyles((theme) => ({
  todo: {
    display: 'flex',
    flexDirection: 'column',
    background: theme.palette.background.default,
    maxHeight: theme.spacing(42),
    overflow: 'hidden auto'
  }
}));


//* Component
export default function LazyFetch() {
  const { pathname } = useRouter();
  const [refresh, setRefresh] = useState(uuid());
  const classes = useStyles();

  return (
    <>
      <DocumentHead title="Lazy CRUD" />

      <Subject variant="h4" color="primary">
        Lazy CRUD
      </Subject>

      <Container disableGutters maxWidth="sm">
        <EditTodoList key={refresh} className={classes.todo} />
      </Container>

      <SyntaxBar source={__WEBPACK_DEFINE__.SOURCE_CODE[pathname]}>
        <Button
          color="secondary"
          size="large"
          variant="text"
          startIcon={(<RefreshIcon />)}
          onClick={() => setRefresh(uuid())}
        >
          Reload
        </Button>
      </SyntaxBar>
    </>
  );
}
