const fs = require('fs');
const path = require('path');
const { DefinePlugin } = require('webpack');

const routerSrc = require('./src/assets/json/src.json');
const basePath = process.env.NODE_ENV === 'production' ? '/knowledge' : '';


const getFiles = (folderPath) => (
  fs.readdirSync(folderPath).reduce(
    ({ items, ...result }, item) => {
      const itemPath = `${folderPath}\\${item}`;
      const lstat = fs.lstatSync(itemPath);

      if (lstat.isDirectory()) {
        items.push(getFiles(itemPath));
      } else {
        const extname = path.extname(item);

        items.push({
          label: item.substring(0, item.length - extname.length),
          type: extname.toLowerCase().substring(1)
        });
      }

      items.sort(({ label: l1 }, { label: l2 }) => (
        l1 < l2 ? -1 : l1 > l2 ? 1 : 0
      ));

      return { ...result, items };
    },
    { label: folderPath.split('\\').pop(), items: [] }
  )
);


/** @type {import('next').NextConfig} */
module.exports = {
  basePath,
  reactStrictMode: false,
  swcMinify: true,

  webpack: (config) => {
    config.module.rules.push({
      test: /\.svg$/,
      use: ['@svgr/webpack']
    });

    config.resolve.alias = {
      '@assets': path.resolve(__dirname, './src/assets'),
      '@components': path.resolve(__dirname, './src/components'),
      '@features': path.resolve(__dirname, './src/features'),
      '@services': path.resolve(__dirname, './src/services'),
      '@utils': path.resolve(__dirname, './src/utils'),

      ...config.resolve.alias
    };

    config.plugins.push(
      new DefinePlugin({
        '__WEBPACK_DEFINE__.BASE_PATH': JSON.stringify(basePath),

        '__WEBPACK_DEFINE__.SOURCE_CODE': JSON.stringify(
          Object.entries(routerSrc).reduce(
            (result, [pathname, tabs]) => ({
              ...result,
              [pathname]: tabs.map(({ title, src: file }) => ({
                title,
                src: fs.readFileSync(path.resolve(__dirname, `./src${file}`), 'utf8')
              }))
            }),
            {}
          )
        ),
        '__WEBPACK_DEFINE__.SOURCE_STRUCTURE': JSON.stringify(
          getFiles(path.resolve(__dirname, './src'))
        )
      })
    );

    return config;
  }
};
