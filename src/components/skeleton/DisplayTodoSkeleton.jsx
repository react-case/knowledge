import PropTypes from 'prop-types';

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Skeleton from '@material-ui/lab/Skeleton';


export default function DisplayTodoSkeleton({ size: length = 10 }) {
  return (
    <>
      {Array.from({ length }).map((_e, i) => (
        <ListItem key={`skeleton_${i}`}>
          <ListItemText
            disableTypography
            primary={(
              <Skeleton variant="text" width="60%" />
            )}
            secondary={(
              <Skeleton variant="text" />
            )}
          />
        </ListItem>
      ))}
    </>
  );
}

DisplayTodoSkeleton.propTypes = {
  size: PropTypes.number
};
