import PropTypes from 'prop-types';
import { Suspense, useRef } from 'react';

import AddIcon from '@material-ui/icons/Add';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import Toolbar from '@material-ui/core/Toolbar';

import EditTodoSkeleton from '@components/skeleton/EditTodoSkeleton';
import fetcher from '@services/todo';
import { generate as uuid } from 'shortid';
import { useLazyFetchData } from '@utils/lazy-hooks';

import EditTodoItem from './EditTodoItem';


export default function EditTodoList({ className }) {
  const LazyTodoBody = useLazyFetchData(fetcher.search, []);
  const bodyRef = useRef();

  return (
    <List
      className={className}
      subheader={(
        <ListSubheader component={Toolbar} variant="dense">
          <IconButton
            color="primary"
            style={{ marginLeft: 'auto' }}
            onClick={() => (
              bodyRef.current?.add({
                content: '',
                done: false,
                dt: (new Date()).toISOString(),
                id: uuid()
              })
            )}
          >
            <AddIcon />
          </IconButton>
        </ListSubheader>
      )}
    >
      <Suspense fallback={(<EditTodoSkeleton size={6} />)}>
        <LazyTodoBody ref={bodyRef}>
          {(uid, data, status) => (
            <EditTodoItem key={uid} {...{ uid, status, data }} />
          )}
        </LazyTodoBody>
      </Suspense>
    </List>
  );
}

EditTodoList.propTypes = {
  className: PropTypes.string
};
