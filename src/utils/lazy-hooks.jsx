import { generate as uuid } from 'shortid';

import {
  Fragment,
  createContext,
  forwardRef,
  lazy,
  useContext,
  useImperativeHandle,
  useMemo,
  useReducer,
  useState
} from 'react';


const LazyContext = createContext(() => null);

export const useLazyFetchDispatch = () => useContext(LazyContext);

export function useLazyAction(uid, importation, onFetch) {
  const dispatch = useLazyFetchDispatch();
  const [fetcher, setFetcher] = useState(() => Promise.resolve());

  return [
    useMemo(() => (
      lazy(() => fetcher.then(importation))
    ), [fetcher]),

    (...e) => (
      setFetcher(
        onFetch(dispatch, ...e)
          .then(() => dispatch({ type: 'commit', options: uid }))
          .catch((err) => (
            console.error(err)
          ))
      )
    )
  ];
}

export const useLazyFetchData = (() => {
  const STATUS = Symbol('STATUS');
  const UID = Symbol('UID');

  const reducer = (state, { type, options }) => {
    switch (type) {
      case 'add': {
        const { method, data } = options;

        state[method]({ ...data, [UID]: uuid(), [STATUS]: 'add' });

        return [...state];
      }
      case 'change':
        return state.map((data) => (
          data[UID] !== options.uid
            ? data
            : {
              ...data,
              [STATUS]: data[STATUS] === 'add' ? 'add' : 'modified',
              ...options.data
            }
        ));

      case 'destroy':
        return state.reduce(
          (result, data) => {
            if (data[UID] !== options) {
              result.push(data);
            } else if (data[STATUS] !== 'add') {
              result.push({ ...data, [STATUS]: 'destroyed' });
            }

            return result;
          },
          []
        );

      case 'commit':
        return state.reduce(
          (result, data) => {
            if (data[UID] !== options) {
              result.push(data);
            } else if (data[STATUS] !== 'destroyed') {
              result.push({ ...data, [STATUS]: 'fetched' });
            }

            return result;
          },
          []
        );

      default:
        return state;
    }
  };

  return (fetcher, deps, BodyElement = Fragment) => (
    useMemo(() => (
      lazy(() => (
        fetcher(...deps)
          .catch((err) => {
            console.error(err);

            return [];
          })
          .then((resultData) => (
            resultData.map((data) => ({
              [UID]: uuid(),
              [STATUS]: 'fetched',
              ...data
            }))
          ))
          .then((initialData) => {
            const WrapperElement = forwardRef(({ children, ...props }, ref) => {
              const [fetchedData, dispatch] = useReducer(reducer, initialData);

              useImperativeHandle(ref, () => ({
                add: (data, method = 'push') => dispatch({ type: 'add', options: { data, method } })
              }), []);

              return (
                <LazyContext.Provider value={dispatch}>
                  <BodyElement {...props}>
                    {fetchedData.map(({ [UID]: uid, [STATUS]: status, ...data }) => children(uid, data, status))}
                  </BodyElement>
                </LazyContext.Provider>
              );
            });

            WrapperElement.displayName = 'FetchBodyWrapper';

            return { default: WrapperElement };
          })
      ))
    ), deps)
  );
})();
