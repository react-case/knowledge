import NextLink from 'next/link';
import PropTypes from 'prop-types';
import { Fragment, useCallback, useMemo, useState } from 'react';
import { useRouter } from 'next/router';

import AppBar from '@material-ui/core/AppBar';
import Avatar from '@material-ui/core/Avatar';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Collapse from '@material-ui/core/Collapse';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import withWidth from '@material-ui/core/withWidth';
import { makeStyles } from '@material-ui/core/styles';

import CloseIcon from '@material-ui/icons/Close';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import MenuBookIcon from '@material-ui/icons/MenuBook';

import cx from 'clsx';

import Logo from './Logo';


const PROP_TYPES = {
  LINK: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
    search: PropTypes.objectOf(PropTypes.any),
    text: PropTypes.string.isRequired
  }),
  GROUP: PropTypes.shape(
    Object.defineProperty({ text: PropTypes.string.isRequired, items: PropTypes.any }, 'items', {
      get: () => PropTypes.arrayOf(
        PropTypes.oneOfType([
          PROP_TYPES.LINK,
          PROP_TYPES.GROUP
        ])
      ).isRequired
    })
  )
};


//* Custom Hooks
const useStyles = makeStyles((theme) => {
  const width = 320;

  return {
    root: {
      position: 'fixed',
      top: 0,
      left: 0,
      height: '100vh',
      flexShrink: 0,
      overflow: 'hidden !important',
      whiteSpace: 'nowrap',
      zIndex: theme.zIndex.appBar + 1,

      '& > ul[role=menu]': {
        display: 'flex',
        flexDirection: 'column',
        overflow: 'hidden auto !important',

        '& > *': {
          width: '100%'
        },
        '& > *[role=footer]': {
          position: 'sticky',
          bottom: 0,
          borderTop: `1px solid ${theme.palette.divider}`,
          marginTop: 'auto',

          '& > div': {
            fontStyle: 'italic',
            width: '100%',

            '& > *:first-child': {
              marginRight: 'auto'
            },
            '& > * + *': {
              marginLeft: theme.spacing(1)
            }
          }
        }
      },
      '&[role=expanded]': {
        width,

        transition: theme.transitions.create('width', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.enteringScreen
        }),
        '& > ul[role=menu] > *[role=subheader]': {
          borderBottom: `1px solid ${theme.palette.divider}`,

          '& > div': {
            justifyContent: 'space-between'
          }
        },
        [theme.breakpoints.only('xs')]: {
          width: '100%'
        }
      },
      '&[role=collapsed]': {
        width: theme.spacing(8) + 1,
        overflowX: 'hidden',

        transition: theme.transitions.create('width', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen
        }),
        [theme.breakpoints.only('xs')]: {
          width: 0
        },
        '& > ul[role=menu]': {
          '& > *[role=subheader] > div': {
            justifyContent: 'center'
          },
          '& > *:not([role=subheader]):not([role=footer])': {
            padding: theme.spacing(1, 0),

            '& > *[role=avatar]': {
              display: 'flex',
              justifyContent: 'center',
              margin: '0 !important',
              width: '100%'
            }
          }
        }
      }
    },
    avatar: {
      background: theme.palette.primary.main,
      color: theme.palette.text.primary,

      '&.on': {
        background: theme.palette.secondary.main
      }
    },
    container: {
      paddingLeft: ({ level = 1 } = {}) => theme.spacing(2 * level),

      '& > [role=avatar]': {
        transform: ({ level = 1 } = {}) => `scale(${Math.pow(0.95, level).toFixed(4)})`
      }
    }
  };
});


//* Components
function NavMenuItem({ level = 1, minify = false, seq, items, pathname, search, text, onGroupClick, onLinkClick }) {
  const { pathname: actived } = useRouter();
  const [expanded, setExpanded] = useState(false);
  const isGroup = Array.isArray(items) && items.length;
  const isSelected = isGroup ? (expanded && !minify) : pathname === actived;
  const classes = useStyles({ level });

  const handleGroupClick = useCallback((e) => {
    // e.currentTarget.scrollIntoView({ behavior: 'smooth', block: 'center' });
    setExpanded(minify ? true : !expanded);
    onGroupClick({ text, items }, !expanded);
  }, [expanded, minify]);

  const [Wrapper, wrapperProps] = useMemo(() => {
    if (!isGroup) {
      const url = new URL(global.document?.location);

      url.pathname = `${__WEBPACK_DEFINE__.BASE_PATH}${pathname}`;
      url.search = (new URLSearchParams(search)).toString();

      return [NextLink, { href: url.toString() }];
    }

    return [Fragment];
  }, []);

  return (
    <>
      <Wrapper {...wrapperProps}>
        <ListItem
          button
          className={classes.container}
          {...(isGroup
            ? { onClick: handleGroupClick }
            : { component: 'a', href: wrapperProps.href, selected: isSelected, onClick: () => onLinkClick({ pathname, search, text }) }
          )}
        >
          <Tooltip key={minify} title={text} {...(!minify && { open: false })}>
            <ListItemAvatar role="avatar">
              <Avatar className={cx(classes.avatar, { on: isSelected })}>
                {seq}
              </Avatar>
            </ListItemAvatar>
          </Tooltip>

          {!minify && (
            <ListItemText
              primaryTypographyProps={{ variant: 'subtitle1' }}
              primary={text}
            />
          )}

          {!minify && isGroup && (
            <ListItemSecondaryAction>
              <IconButton color="default" onClick={handleGroupClick}>
                {expanded
                  ? (<ExpandLess />)
                  : (<ExpandMore />)
                }
              </IconButton>
            </ListItemSecondaryAction>
          )}
        </ListItem>
      </Wrapper>

      {isGroup && !minify && (
        <Collapse in={expanded}>
          {items.map((item, i) => (
            <NavMenuItem
              key={item.text}
              level={level + 1}
              seq={`${seq}.${i + 1}`}
              {...{ minify, onGroupClick, onLinkClick }}
              {...item}
            />
          ))}
        </Collapse>
      )}
    </>
  );
}

function NavigationDrawer({ items, open, width, onMenuToggle, onNavItemClick }) {
  const classes = useStyles();

  return (
    <ClickAwayListener {...(width === 'xs' && { mouseEvent: false, touchEvent: false })} onClickAway={() => onMenuToggle(false)}>
      <Drawer
        role={open ? 'expanded' : 'collapsed'}
        variant="permanent"
        className={classes.root}
        PaperProps={{
          role: 'menu',
          component: List,
          subheader: (
            <AppBar role="subheader" position="sticky" color="transparent" elevation={0} component={ListSubheader} disableGutters>
              <Toolbar disableGutters={!open}>
                {open && (
                  <Logo onClick={() => onMenuToggle(false)} />
                )}

                {width !== 'xs' && !open && (
                  <IconButton color="default" onClick={() => onMenuToggle(true)}>
                    <MenuBookIcon />
                  </IconButton>
                )}

                {width === 'xs' && open && (
                  <IconButton color="default" onClick={() => onMenuToggle(false)}>
                    <CloseIcon />
                  </IconButton>
                )}
              </Toolbar>
            </AppBar>
          )
        }}
      >
        {items.map((item, i) => (
          <NavMenuItem
            key={item.text}
            seq={i + 1}
            minify={!open}
            {...item}
            onGroupClick={() => onMenuToggle(true)}
            onLinkClick={(e) => {
              onMenuToggle(false);
              onNavItemClick?.(e);
            }}
          />
        ))}
      </Drawer>
    </ClickAwayListener>
  );
}

NavigationDrawer.propTypes = {
  actived: PropTypes.string,
  open: PropTypes.bool.isRequired,

  onMenuToggle: PropTypes.func.isRequired,
  onNavItemClick: PropTypes.func,

  items: PropTypes.arrayOf(
    PropTypes.oneOfType([
      PROP_TYPES.LINK,
      PROP_TYPES.GROUP
    ])
  ).isRequired
};

export default withWidth()(NavigationDrawer);
