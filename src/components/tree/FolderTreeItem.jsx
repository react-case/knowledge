import PropTypes from 'prop-types';
import { useImperativeHandle, useRef } from 'react';

import Chip from '@material-ui/core/Chip';
import TreeItem from '@material-ui/lab/TreeItem';
import Typography from '@material-ui/core/Typography';

import FileTreeItem from './FileTreeItem';


export default function FolderTreeItem({ label, items }) {
  const ref = useRef();
  const count = ref.current || 1;

  useImperativeHandle(ref, () => count + 1);

  return (
    <TreeItem
      nodeId={label}
      label={(
        <Typography variant="subtitle1" color="textSecondary" style={{ display: 'flex', alignItems: 'center' }}>
          {label}

          <Chip size="small" color="secondary" label={count} style={{ marginLeft: 12 }} />
        </Typography>
      )}
    >
      {items.map(({ label: childLabel, type, items: childItems }) => (
        Array.isArray(childItems)
          ? (
            <FolderTreeItem key={childLabel} label={childLabel} items={childItems} />
          )
          : (
            <FileTreeItem key={childLabel} label={childLabel} type={type} />
          )
      ))}
    </TreeItem>
  );
}

FolderTreeItem.propTypes = {
  label: PropTypes.string.isRequired,

  items: PropTypes.arrayOf(
    PropTypes.object.isRequired
  )
};
