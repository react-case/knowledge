import { createContext, useContext, useMemo, useState } from 'react';

const SelectionContext = createContext({
  selecteds: new Set(),
  onSelect: () => null
});

export const useSelection = () => useContext(SelectionContext);

export const useProviderValue = () => {
  const [selecteds, setSelecteds] = useState(new Set());

  return useMemo(() => ({
    selecteds,

    onSelect(isSelected, id) {
      selecteds[isSelected ? 'add' : 'delete'](id);
      setSelecteds(new Set(selecteds));
    }
  }), [selecteds]);
};

export default SelectionContext.Provider;