import PropTypes from 'prop-types';
import { forwardRef } from 'react';

import Button from '@material-ui/core/Button';


const TextTransformButton = forwardRef(({ textTransform = 'capitalize', ...props }, ref) => (
  <Button ref={ref} {...props} style={{ ...props.style, textTransform }} />
));

TextTransformButton.displayName = 'TextTransformButton';

TextTransformButton.propTypes = {
  textTransform: PropTypes.oneOf(['capitalize', 'uppercase']),
};

export default TextTransformButton;
