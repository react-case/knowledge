import PropTypes from 'prop-types';

import AppBar from '@material-ui/core/AppBar';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import { makeStyles } from '@material-ui/core/styles';

import MenuBookIcon from '@material-ui/icons/MenuBook';

import cx from 'clsx';

import Logo from './Logo';


//* Custom Hooks
const useStyles = makeStyles((theme) => ({
  root: {
    background: theme.palette.background.default,

    '& > [role=toolbar]': {
      height: theme.spacing(8),

      '& > [role=toggle]': {
        [theme.breakpoints.up('sm')]: {
          display: 'none'
        }
      },
      '& > * + *': {
        marginLeft: theme.spacing(1)
      }
    }
  }
}))


//* Component
export default function HeaderBar({ action, className, disableLogo = false, onMenuOpen }) {
  const classes = useStyles();

  return (
    <AppBar position="sticky" className={cx(classes.root, className)}>
      <Toolbar role="toolbar">
        <IconButton role="toggle" color="default" onClick={onMenuOpen}>
          <MenuBookIcon />
        </IconButton>

        {!disableLogo && (
          <Logo />
        )}

        {action}
      </Toolbar>
    </AppBar>
  );
}

HeaderBar.propTypes = {
  action: PropTypes.node,
  className: PropTypes.string,
  disableLogo: PropTypes.bool,
  onMenuOpen: PropTypes.func.isRequired
};
