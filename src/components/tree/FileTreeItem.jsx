import PropTypes from 'prop-types';

import SvgIcon from '@material-ui/core/SvgIcon';
import TreeItem from '@material-ui/lab/TreeItem';
import Typography from '@material-ui/core/Typography';

import { useSelection } from '@utils/selection-context';

import JS_ICON from '@assets/images/js-logo.svg';
import JSON_ICON from '@assets/images/json-logo.svg';
import REACT_ICON from '@assets/images/react-logo.svg';
import SVG_ICON from '@assets/images/svg-logo.svg';


export default function FileTreeItem({ label, type }) {
  const { selecteds, onSelect } = useSelection();
  const nodeId = `${label}.${type}`;
  const isSelected = selecteds.has(nodeId);

  return (
    <TreeItem
      nodeId={nodeId}
      label={(
        <Typography variant="body1" color={isSelected ? 'primary' : 'textPrimary'}>
          {nodeId}
        </Typography>
      )}
      icon={type === 'jsx'
        ? (<SvgIcon fontSize="large" viewBox="0 0 512 512" color="primary" component={REACT_ICON} />)
        : type === 'js'
          ? (<SvgIcon fontSize="large" viewBox="0 0 24 24" color="primary" component={JS_ICON} />)
          : type === 'json'
            ? (<SvgIcon fontSize="large" viewBox="0 0 32 32" color="primary" component={JSON_ICON} />)
            : type === 'svg'
              ? (<SvgIcon fontSize="large" viewBox="0 0 32 32" color="primary" component={SVG_ICON} />)
              : null
      }
      onClick={() => onSelect(!isSelected, nodeId)}
    />
  );
}

FileTreeItem.propTypes = {
  label: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['js', 'json', 'jsx', 'svg']).isRequired
};
