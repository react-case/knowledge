import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';


export default withStyles((theme) => ({
  root: {
    marginTop: theme.spacing(2),
    fontWeight: 'bolder !important',
    zIndex: 1
  }
}))((props) => (
  <Typography {...props} paragraph color="secondary" />
));
