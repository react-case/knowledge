import DocumentHead from '@features/app/DocumentHead';


export default function HomeIndex() {
  return (
    <>
      <DocumentHead title="React Knowledge" />
    </>
  );
}
